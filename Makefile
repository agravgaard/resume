name_cl = cover-letter
cl_dir = ./cover_letter
dict = mywords.utf-8.add

# options: flip, crop
img_to_use = flip

tex_input = ".:$(cl_dir):"

all: $(name_cl).pdf

README.md: index.md text/*.txt
	m4 -I./text/ $< > README.md

resume-md.pdf: README.md test.css me_circ.png pandoc-defaults.yaml
	pandoc --defaults ./pandoc-defaults.yaml --css test.css -o resume-md.pdf
	# --pdf-engine-opt='--page-width' --pdf-engine-opt='210mm' --pdf-engine-opt='--page-height' --pdf-engine-opt='600mm'

$(name_cl).pdf: $(cl_dir)/$(name_cl).tex $(cl_dir)/*.tex text/*.txt $(cl_dir)/*.cls me.png
	TEXINPUTS=$(tex_input) latexmk -pdflua -interaction=batchmode -shell-escape $<

once_cover_letter: $(cl_dir)/$(name_cl).tex $(cl_dir)/*.tex me.png
	TEXINPUTS=$(tex_input) lualatex -shell-escape $<

luxion: $(name_cl)-luxion.pdf
uniqfeed: $(name_cl)-uniqfeed.pdf

lunar: $(name_cl)-lunar.pdf

ubereats: $(name_cl)-ubereats.pdf

systematic: $(name_cl)-systematic.pdf

gitlab: $(name_cl)-gitlab.pdf

vestas: $(name_cl)-vestas.pdf

cgi: $(name_cl)-cgi.pdf

roku: $(name_cl)-roku.pdf

eficode: $(name_cl)-eficode.pdf

evenito: $(name_cl)-evenito.pdf

opensystems: $(name_cl)-opensystems.pdf

deloitte: $(name_cl)-deloitte.pdf

terma: $(name_cl)-terma.pdf

cryptomathics: $(name_cl)-cryptomathics.pdf

danskebank: $(name_cl)-danskebank.pdf

mindwayai: $(name_cl)-mindwayai.pdf

$(name_cl)-%.pdf: $(cl_dir)/$(name_cl).tex $(cl_dir)/*.tex text/*.txt $(cl_dir)/*.cls me.png
	echo $@
	$(eval suff := $(shell echo $@ | sed -nE 's/$(name_cl)-([a-z]+).pdf/\1/p'))
	echo $(suff)
	sed 's/cover-letter\.txt/cover-letter-$(suff).txt/g' $(cl_dir)/$(name_cl).tex > $(name_cl)-$(suff).tex
	TEXINPUTS=$(tex_input) latexmk -pdflua -interaction=batchmode -shell-escape $(name_cl)-$(suff).tex
	rm $(name_cl)-$(suff).tex

$(dict): ci/my_words.txt
	cp ci/my_words.txt $@

$(dict).spl: $(dict)
	vim "+mkspell! $@ $^" +qall

vimrc: $(dict) $(dict).spl
	echo "set spelllang=en_gb" > .vimrc
	echo "set spellfile=$(dict)" >> .vimrc
	echo "set wrap linebreak nolist" >> .vimrc
	echo "set textwidth=70" >> .vimrc

me_crop.png: me_orig.png
	convert -crop "370x370+60+20" me_orig.png me_crop.png

me_flip.png: me_crop.png
	convert -transpose me_crop.png me_T.png
	convert -rotate 90 me_T.png me_flip.png
	rm me_T.png

me.png: me_$(img_to_use).png
	cp me_$(img_to_use).png me.png

me_circ.png: me.png
	convert me.png -gravity Center \( -size 300x300 xc:Black -fill White -draw 'circle 150 150 150 1' -alpha Copy \) -compose CopyOpacity -composite -trim me_circ.png
	convert -resize 200 me_circ.png me_circ.png

clean:
	$(RM) *.o *.log *.aux *.bbl *.log *.toc *.out *.blg *.fls *.fdb_latexmk *.xdv
