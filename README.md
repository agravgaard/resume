# Andreas Gravgaard Andersen
## Software Engineer
### Education

 - **Ph.D. in Medical Physics**, Aarhus University, 2016-2021
 - **M.Sc. in Physics**, Aarhus University, 2015-2017
 - **B.Sc. in Physics**, Aarhus University, 2012-2015


| <andreasga22@gmail.com> | [LinkedIn](https://www.linkedin.com/in/agravgaard) | [Twitter](https://twitter.com/Andreas_GA) | [GitLab](https://gitlab.com/agravgaard) | [GitHub](https://github.com/agravgaard) | [Latest CV](https://agravgaard.gitlab.io/resume/resume.pdf) |
| :---: | :---: | :---: | :---: | :---: | :---: |

A motivated software developer with a background in physics and highly skilled
in problem-solving. Advanced knowledge of C++ with a particular focus on
performance tuning and reliability. [GitLab awarded me MVP for release
v13.9](https://about.gitlab.com/releases/2021/02/22/gitlab-13-9-released/#mvp).
I am looking for a company that will challenge me to find efficient and robust
solutions to interesting problems.


I love learning and self-improvement. I often experiment with my
computers/servers, automating and optimizing my systems and learning new
programming languages or technologies/architectures. My less technical
spare-time activities include playing guitar and violin, sailing, and diving
with my partner.


### DevOps Engineer, Team Lead/Consultant, Accenture, *Zürich, Feb 2023 - now* [1.5 YoE]

My main responsibility was Identity & Access Management (IAM) for B2B customers at a large Swiss Telecom provider for
~1M users. I contributed to the existing attribute-based access control (ABAC) and a new Role-based access control
(RBAC) model, through JavaScript scripting with Ping (formerly Forgerock) services and custom Java Spring Boot based
microservices.

I had a considerable role in migrating old Jenkins pipelines to GitLab CI as the client transitioned from BitBucket to
GitLab. I wrote CI/CD pipelines to lint, test, security scan, build and deploy both container images and
application/library packages for the Java microservices, typescript frontend and the custom JavaScript plugins.

I managed deployments of services and production environments with Docker, Jenkins, Ansible, and Linux command line
tools. I participated in the on-call rotation which included Site Reliability Engineering (SRE) work, such as setting up
log rotation, cleaning up old data in databases and creating a microservice for database observability exposing a
Prometheus scraping endpoint for Grafana reporting.


### DevOps Consultant, Eficode, *Aarhus, Aug 2021 - Jan 2023* [1.5 YoE]

The primary responsibility of this role was to advance the clients' software
delivery methods. I have developed, optimized, and implemented continuous
integration and delivery (CI/CD), automation, network security, and
infrastructure-as-code solutions.

I have used Ansible and Terraform to write automatic deployments for clients'
services like Artifactory, Nginx, HAproxy, and Azure virtual machines (VMs). I
have written CI/CD pipelines to format, lint and test scripts and integrations
in GitHub actions and GitLab CI. The integrations have included virtualization
and containerization with Vagrant, Docker, Podman and Kubernetes. Additionally,
as a DevOps consultant, I have learned and taught about ways of project
management, including GitOps, Agile, Lean, SAFe, Team Topologies, and Site
Reliability Engineering (SRE) and their benefits and drawbacks.


### Programming Languages:
C++, Javascript, C, Java, Rust, Bash, Go, Python, OpenCL, R, CUDA, LaTeX, Matlab

### Tools & Frameworks:
Linux/Unix tools, Docker, git, Kubernetes, Ansible, GitLab CI, Terraform,
GitHub Actions, Flux CD, Nginx, Jenkins, HAproxy, OracleDB (PL/SQL), PostgreSQL,
Microsoft SQL, Prometheus, Grafana, Thanos, AWS, Azure, SpringBoot, Angular

### Languages:
English, Danish, German, Norwegian, Swedish, French

<p class="new-page"></p>

### Ph.D. Student, Aarhus University & Harvard University, *Aarhus/Boston, 2016 - 2021*

I created and investigated different approaches to account for inter-fractional
motion in proton therapy. Specifically, I analyzed beam angle robustness to
radiation dosage and proton ranges. Ultimately, the project would provide fast
and reliable methods for monitoring the influence of inter-fractional changes
on the dose plan.

During my PhD, I managed the projects independently. I collaborated with other
researchers on some projects. In particular, I worked closely with one
co-located PhD student and several others during my six-month stay at Harvard
University. Additionally, I supervised six engineering students through their
bachelor projects.


### Volunteering Experience
 - **Treasurer**, Residents Union, Vilh. Kiers Dormitory, *Aarhus, 2016 - 2019*
 - **Treasurer & Foreman**, Music Union, Vilh. Kiers Dormitory, *Aarhus, 2015 - 2017*
 - **Tutor**, The Mat/Fys-Tutorgroup, Aarhus University, *Aarhus, 2013 - 2013*

### Selected Projects
### [CBCTRecon](https://www.gitlab.com/agravgaard/cbctrecon)

An application for scatter correction of cone-beam computed
tomographies. This project was a central part of my PhD.  The
project is written in C++ using Qt for the user interface. It
takes advantage of different APIs and libraries, including
OpenCL, RTK, ITK and Plastimatch.


### [Turing Pi Maintainer](https://gitlab.com/agravgaard/turingpi-maintainer)

A project to create a self-healing Kubernetes (k8s) cluster. Seven raspberry pi
compute modules were placed in one board with a network controller. After
flashing them with a minimal Manjaro image for ARM, I [provisioned k3s on them
and enabled i2c using ansible
playbooks](https://github.com/agravgaard/k3s-ansible). I had the issue that one
or more nodes would occasionally be unresponsive. Luckily, the board had an i2c
interface to control each node's power switch. So I wrote a Go application that
could send the power off and on command for the unresponsive node. I wrapped it
in a Dockerfile, ran it as a DaemonSet on k8s and used the k8s Go API for
leader election and readiness checks. Finally, I decided to use a GitOps
approach for the project. The k8s cluster state is managed with FluxCD, and the
desired state is in [another repo](https://gitlab.com/agravgaard/flux-gitops).


### Other Open-Source Projects
 - [Kubernetes manifests for FluxCD, controlling my private cluster setup with GitOps](https://gitlab.com/agravgaard/flux-gitops)
 - [Ansible Collection to deploy Kubernetes nodes that connect the Kubelet to Talos Linux nodes](https://gitlab.com/agravgaard/ansible-collection-k8s)
 - [Ansible scripts to deploy Artifactory using Docker, Podman, K8s and Yum on RHEL](https://gitlab.com/agravgaard/deploy-simulator)
 - [A Rust learning project, experimenting with options trading math](https://gitlab.com/agravgaard/algotrading)
 - [A tiny zero-downtime demo\* in Go with Kubernetes for teaching purposes](https://gitlab.com/agravgaard/ZeroDown)
 - [A Ray-tracing learning project in C++](https://gitlab.com/agravgaard/my-ray-tracer)
 - [A small Terraform demo\* connecting an AWS EC2 instance with Cloudflare DNS](https://gitlab.com/agravgaard/my-terraform-project)
 - [This resume, with a CI pipeline to check grammar, produce PDFs and publish online](https://gitlab.com/agravgaard/resume)
 - A few different R
   [1](https://gitlab.com/agravgaard/bayesiancourse2020),
   [2](https://gitlab.com/agravgaard/first_patient_dose_analysis),
   [3](https://gitlab.com/agravgaard/weplplotting_singlepatient),
   [4](https://gitlab.com/agravgaard/weplplotting),
   [5](https://gitlab.com/agravgaard/catphansegmentanalysis),
   [6](https://gitlab.com/agravgaard/gammastatistics),
   Python
   [1](https://gitlab.com/dcpt-research/pyvoistats),
   [2](https://gitlab.com/dcpt-research/doserecalc)
   and C++
   [1](https://github.com/agravgaard/QtPlotBraggPeak),
   [2](https://github.com/agravgaard/guiPMC)
   data analysis projects from my PhD
 - [My PhD Thesis in LaTeX, with a CI pipeline to check grammar and self-plagiarism and produce PDFs](https://gitlab.com/agravgaard/phd-thesis)
 - Various contributions include
   [the GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/1955),
   [ITK](https://github.com/InsightSoftwareConsortium/ITK/pull/2755),
   [RTK](https://github.com/SimonRit/RTK/pulls?q=is%3Apr+author%3Aagravgaard+is%3Aclosed),
   [DCMTK](https://github.com/DCMTK/dcmtk/pull/9) and
   [rocm-arch](https://github.com/rocm-arch/rocm-arch/pulls?q=is%3Apr+author%3Aagravgaard+is%3Aclosed)

\*Performed live for clients

<p class="new-page"></p>

## Publications

**Beam angle evaluation to improve inter-fraction motion robustness in pelvic lymph node irradiation with proton therapy**\
**Andreas G. Andersen**, Oscar Casares-Magaz, Jørgen B. B. Petersen, Jakob Toftegaard, Lise Bentzen, Sara Thörnqvist & Ludvig P. Muren
— *Acta Oncologica (2017), 56:6, 846-852, [doi-link](https://doi.org/10.1080/0284186X.2017.1317108)*

**Evaluation of an a priori scatter correction algorithm for cone-beam CT projections in photon vs. proton therapy gantries**\
**Andreas G. Andersen**, Yang-Kyun Park, Ulrik V. Elstrøm, Jørgen B. B. Petersen, Gregory C. Sharp, Brian Winey, Lei Dong & Ludvig P. Muren
— *Physics and Imaging in Radiation Oncology (2020), 16, 89-94, [doi-link](https://doi.org/10.1016/j.phro.2020.09.014)*

**Towards range-guidance in proton therapy to detect organ motion-induced dose degradations**\
Kia Busch\*, **Andreas G. Andersen**\*, Jørgen B. B. Petersen, Stine E. Petersen, Heidi S. Rønde, Lise Bentzen, Sara Pilskog, Peter Skyt, Ole Nørrevang & Ludvig P. Muren
— *Biomedical Physics & Engineering Express (2022), 8, 025018, [doi-link](https://doi.org/10.1088/2057-1976/ac5151)*\
\*Joint authorship

**A method for evaluation of proton plan robustness towards inter-fractional motion applied to pelvic lymph node irradiation**\
**Andreas G. Andersen**, Oscar Casares-Magaz, Ludvig P.  Muren, Jakob Toftegaard, Lise Bentzen, Sara Thörnqvist & Jørgen B. B. Petersen
— *Acta Oncologica (2015), 54:9, 1643-1650, [doi-link](https://doi.org/10.3109/0284186X.2015.1067720)*

**Evaluating the influence of organ motion during photon vs. proton therapy for locally advanced prostate cancer using biological models**\
Kia Busch, **Andreas G.  Andersen**, Oscar Casares-Magaz, Jørgen B. B. Petersen, Lise Bentzen, Sara Thörnqvist & Ludvig P. Muren
— *Acta Oncologica (2017), 56:6, 839-845, [doi-link](https://doi.org/10.1080/0284186X.2017.1317107)*

**A biological modelling based comparison of radiotherapy plan robustness using photons vs protons for focal prostate boosting**\
Jesper Pedersen, Oscar Casares-Magaz, Jørgen B. B. Petersen, Jarle Rørvik, Lise Bentzen, **Andreas G.  Andersen** & Ludvig P.Muren
— *Physics and Imaging in Radiation Oncology (2018), 6, 101-105, [doi-link](https://doi.org/10.1016/j.phro.2018.06.002.)*

**On-line dose-guidance to account for inter-fractional motion during proton therapy**\
Kia Busch, Ludvig P. Muren, Sara Thörnqvist, **Andreas G. Andersen**, Jesper Pedersen, Lei Dong & Jørgen B. B. Petersen
— *Physics and Imaging in Radiation Oncology (2019), 9, 7-13, [doi-link](https://doi.org/10.1016/j.phro.2018.11.009)*
