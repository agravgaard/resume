### DevOps Consultant, Eficode, *Aarhus, Aug 2021 - Jan 2023* [1.5 YoE]

The primary responsibility of this role was to advance the clients' software
delivery methods. I have developed, optimized, and implemented continuous
integration and delivery (CI/CD), automation, network security, and
infrastructure-as-code solutions.

I have used Ansible and Terraform to write automatic deployments for clients'
services like Artifactory, Nginx, HAproxy, and Azure virtual machines (VMs). I
have written CI/CD pipelines to format, lint and test scripts and integrations
in GitHub actions and GitLab CI. The integrations have included virtualization
and containerization with Vagrant, Docker, Podman and Kubernetes. Additionally,
as a DevOps consultant, I have learned and taught about ways of project
management, including GitOps, Agile, Lean, SAFe, Team Topologies, and Site
Reliability Engineering (SRE) and their benefits and drawbacks.
