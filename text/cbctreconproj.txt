### [CBCTRecon](https://www.gitlab.com/agravgaard/cbctrecon)

An application for scatter correction of cone-beam computed
tomographies. This project was a central part of my PhD.  The
project is written in C++ using Qt for the user interface. It
takes advantage of different APIs and libraries, including
OpenCL, RTK, ITK and Plastimatch.
