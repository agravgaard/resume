
\letter{%
   Terma
}

\opening{Dear MLOps team @ Terma}

I would love to join you in maintaining and improving your platform and services.

I am a physicist by education but have primarily worked with software development for the past ${\sim}5$ years. I am thus excellent at problem-solving and hope to help you find robust and efficient solutions to your challenges.

I am passionate about open-source and have made almost all of my work available open-source. In addition, I enjoy building automation pipelines for my projects.

\begin{adjustbox}{tabular=p{.25\pagewidth}|p{.50\pagewidth}, center}
    What you seek: & What I offer: \\ & \\
    %%
    Programming and debugging in scripting languages
    & I have worked extensively with scripting languages such as Bash, Python, R and Matlab for 8+ years. \\ & \\
    %%
Secure cloud, Edge and IoT architectures
    & In my spare time, I have worked with Raspberry Pi's to set up network communication between them and my personal server over OpenVPN\\ & \\
    %%
Linux system administration
    & I work on Linux daily and has done so for almost 10 years. I enjoy tinkering and tuning the system. \\ & \\
    %%
CI/CD pipelines and tools (I.e., GitLab CI/CD, or equivalent)
    & I have set up GitLab CI pipelines for most of my projects; almost all are open source. \\ & \\
    %%
Containers and containerization (Docker, Kubernetes)
    & I have used docker extensively for testing and CI of my PhD project and several spare-time projects. I hope to set up my own Kubernetes cluster when I receive a Turing Pi board later this month. \\ & \\
    %%
Webservices and microservice management
    & I recently had a merge request accepted into the gitlab-runner service regarding GPU forwarding to docker. \\ & \\
    %%
High-Performance Computing (SLURM, GPUs, ...)
    & I have been working with GPGPU compute throughout my PhD (OpenCL, CUDA and SYCL). Additionally, the compute server I had access to used TORQUE for job management (instead of SLURM). \\ & \\
    %%
Administrating data science stacks such as JupyterHub, Apache Spark and Apache Airflow
    & I have been running my own instances of JupyterHub and RStudio-server on my personal server, which I have used from my laptop and occasionally created users and granted access to friends.
    %%
\end{adjustbox}

\closing{%
   Best regards\\
   Andreas Gravgaard Andersen}
