---
stages:
  - prepare
  - test
  - build
  - coverage

build_mypandoc:
  stage: prepare
  image: docker:27.1.2
  variables:
    DOCKER_TLS_CERTDIR: ""
  services:
    - docker:27.1.2-dind
  before_script:
    - docker login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY
    - export DOCKER_IMAGE="$CI_REGISTRY_IMAGE/mypandoc:$CI_COMMIT_REF_SLUG"
    - |
      if [ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]; then
        export DOCKER_IMAGE="$CI_REGISTRY_IMAGE/mypandoc:latest"
      fi
  script:
    - docker build --pull -t "${DOCKER_IMAGE}" ./ci/
    - docker image push "${DOCKER_IMAGE}"
  only:
    changes:
      - ci/Dockerfile
      - .gitlab-ci.yml
  needs: []

test_readme:
  stage: test
  image: alpine:latest
  before_script:
    - apk add m4
  script:
    - m4 -I./text/ index.md > tmp.md
    - cmp README.md tmp.md
  needs: []

pandoc_pdf:
  stage: build
  image:
    name: ${CI_REGISTRY_IMAGE}/mypandoc:latest
    # name: pandoc/core:2.17-ubuntu
    entrypoint: [""]
  script:
    - make resume-md.pdf
    - mv resume-md.pdf resume-no-image.pdf
  artifacts:
    paths:
      - resume-no-image.pdf
  needs:
    - job: test_readme
      optional: false
    - job: build_mypandoc
      optional: true

pandoc_pdf_with_image:
  stage: build
  image:
    name: ${CI_REGISTRY_IMAGE}/mypandoc:latest
    # name: pandoc/core:2.17-ubuntu
    entrypoint: [""]
  script:
    - sed -i 's/header-no-image/header/' index.md
    - make resume-md.pdf
  artifacts:
    paths:
      - resume-md.pdf
  needs:
    - job: test_readme
      optional: false
    - job: build_mypandoc
      optional: true

compile_pdf:
  stage: build
  image: aergus/latex
  allow_failure: true
  script:
    - make
    - make eficode
  artifacts:
    paths:
      - cover-letter*.pdf
  needs: []

make_langtool_docker:
  stage: prepare
  image: docker:27.1.2
  variables:
    DOCKER_TLS_CERTDIR: ""
  services:
    - docker:27.1.2-dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - apk add make --no-cache
  script:
    - make mywords.utf-8.add
    - cp mywords.utf-8.add ci/langtool_docker/my_words.txt
    - docker build --pull -t "$CI_REGISTRY_IMAGE":langtool ci/langtool_docker
    - docker push "$CI_REGISTRY_IMAGE":langtool
  only:
    changes:
      - ci/my_words.txt
      - ci/langtool_docker
  needs: []

language_tools_ngrams:
  stage: test
  image: docker:27.1.2
  tags:
    - linux
    - docker
    - ngrams
  variables:
    DOCKER_TLS_CERTDIR: ""
  services:
    - docker:27.1.2-dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - >
      if [ ! -d /ngrams ]; then
        mkdir /ngrams
        wget https://languagetool.org/download/ngram-data/ngrams-en-20150817.zip
        apk add unzip --no-cache
        unzip ./ngrams-en-20150817.zip -d /ngrams
        rm -f ngrams-en-20150817.zip
      fi
    - docker run -d --name languagetool -p 8081:8010 -v /ngrams:/ngrams:ro --restart=unless-stopped "$CI_REGISTRY_IMAGE":langtool
    - apk add python3 py3-pip --no-cache
    - python -m venv ./venv
    - . ./venv/bin/activate
    - pip install pylanguagetool==0.10.0 beautifulsoup4 setuptools
    - mkdir -p ~/.config
    - cp ./ci/pyLanguagetool.conf ~/.config/
    - >
      for f in ./text/*.txt ; do
        sed -i 's/\\medskip//g' $f
        sed -i 's/{\\sim}/~/g' $f
        sed -i -E 's/\\href\{[^}]+\}\{([^}]+)\}/\1/g' $f # Keep only second href arg
        sed -i -E 's/\\[a-z]+\{([^}]+)\}/\1/g' $f # only keep content of commands
        sed -i 's/\\\@\././g' $f # period markers
        sed -i 's/\$//g' $f # Make equations plain text
        sed -i 's/\\//g' $f # remove remaining back-slashes
        echo "Running languagetools on $f"
        pylanguagetool --no-color -a http://docker:8081/v2/ < $f || true
      done
  after_script:
    - docker container stop languagetool
    - docker container rm languagetool
  when: manual

language_tools:
  stage: test
  image: docker:27.1.2
  tags:
    - linux
    - docker
  variables:
    DOCKER_TLS_CERTDIR: ""
  services:
    - docker:27.1.2-dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker run -d --name languagetool -p 8081:8010 --restart=unless-stopped "$CI_REGISTRY_IMAGE":langtool
    - apk add python3 py3-pip --no-cache
    - python -m venv ./venv
    - . ./venv/bin/activate
    - pip install pylanguagetool==0.10.0 beautifulsoup4 setuptools
    - mkdir -p ~/.config
    - cp ./ci/pyLanguagetool.conf ~/.config/
    - >
      for f in ./text/*.txt ; do
        sed -i 's/\\medskip//g' $f
        sed -i 's/{\\sim}/~/g' $f
        sed -i -E 's/\\href\{[^}]+\}\{([^}]+)\}/\1/g' $f # Keep only second href arg
        sed -i -E 's/\\[a-z]+\{([^}]+)\}/\1/g' $f # only keep content of commands
        sed -i 's/\\\@\././g' $f # period markers
        sed -i 's/\$//g' $f # Make equations plain text
        sed -i 's/\\//g' $f # remove remaining back-slashes
        echo "Running languagetools on $f"
        pylanguagetool --no-color -a http://docker:8081/v2/ < $f || true
      done
  after_script:
    - docker container stop languagetool
    - docker container rm languagetool
  only:
    changes:
      - text/*


pages:
  stage: coverage
  image: alpine:latest
  script:
    - mkdir -p public
    - mv resume-md.pdf ./public/resume.pdf
    - mv resume-no-image.pdf ./public/resume-no-image.pdf
    # - mv cover-letter*.pdf ./public/
  artifacts:
    paths:
      - public
  needs:
    # - job: compile_pdf
    #   artifacts: true
    - job: pandoc_pdf
      artifacts: true
    - job: pandoc_pdf_with_image
      artifacts: true
