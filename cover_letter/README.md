# Adapted version of friggeri-letter

This is a LaTeX template for a cover letter to go with a resume created with the [friggeri-cv resume template](http://www.latextemplates.com/template/friggeri-resume-cv).

## The letter

All resumes should have a cover letter (so I'm told). It would be inconsistent to stick a normal LaTeX letter infront of a friggeri resume. Also, a normal LaTeX letter is not as eye-catching.

This letter class uses the same eye catching header as the Friggeri CV, and uses the Roboto font.

![letter-printscreen][letter-printscreen]

## Usage

* Install the fonts.
* Have the `.cls` file(s) in the same folder as your `.tex` file(s). (There's probably a way to install them properly. I don't know what that is.)
* Start with the example `.tex` file(s) and modify them to suit your needs.
* This template needs to be compiled with XeLaTeX

## Acknowledgements and Copyleft

This code was adapted from [mdavis-xyz/friggeri-letter](https://github.com/mdavis-xyz/friggeri-letter)
which was adapted from the original code written by [Adrien Friggeri](http://www.friggeri.net/) under the MIT License, printed in full in [LICENSE.txt](LICENSE.txt).

[letter-printscreen]: http://i.imgur.com/ky4uiUV.png
