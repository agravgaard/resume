FROM gitpod/workspace-full

# Install LaTeX
RUN echo "deb http://us.archive.ubuntu.com/ubuntu/ groovy main universe" | sudo tee /etc/apt/sources.list && \
    sudo apt-get -q update && \
    sudo apt-get install -yq texlive-full inotify-tools latex2rtf && \
    sudo rm -rf /var/lib/apt/lists/*
