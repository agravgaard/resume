include(`text/header-no-image.txt')

| <andreasga22@gmail.com> | [LinkedIn](https://www.linkedin.com/in/agravgaard) | [Twitter](https://twitter.com/Andreas_GA) | [GitLab](https://gitlab.com/agravgaard) | [GitHub](https://github.com/agravgaard) | [Latest CV](https://agravgaard.gitlab.io/resume/resume.pdf) |
| :---: | :---: | :---: | :---: | :---: | :---: |

include(`text/profile.txt')

include(`text/aboutme.txt')

include(`text/accenture-exp.txt')

include(`text/efiexp.txt')

### Programming Languages:
C++, Javascript, C, Java, Rust, Bash, Go, Python, OpenCL, R, CUDA, LaTeX, Matlab

### Tools & Frameworks:
Linux/Unix tools, Docker, git, Kubernetes, Ansible, GitLab CI, Terraform,
GitHub Actions, Flux CD, Nginx, Jenkins, HAproxy, OracleDB (PL/SQL), PostgreSQL,
Microsoft SQL, Prometheus, Grafana, Thanos, AWS, Azure, SpringBoot, Angular

### Languages:
English, Danish, German, Norwegian, Swedish, French

<p class="new-page"></p>

include(`text/phdexp.txt')

### Volunteering Experience
 - **Treasurer**, Residents Union, Vilh. Kiers Dormitory, *Aarhus, 2016 - 2019*
 - **Treasurer & Foreman**, Music Union, Vilh. Kiers Dormitory, *Aarhus, 2015 - 2017*
 - **Tutor**, The Mat/Fys-Tutorgroup, Aarhus University, *Aarhus, 2013 - 2013*

### Selected Projects
include(`text/cbctreconproj.txt')

include(`text/turingpiproj.txt')

### Other Open-Source Projects
 - [Kubernetes manifests for FluxCD, controlling my private cluster setup with GitOps](https://gitlab.com/agravgaard/flux-gitops)
 - [Ansible Collection to deploy Kubernetes nodes that connect the Kubelet to Talos Linux nodes](https://gitlab.com/agravgaard/ansible-collection-k8s)
 - [Ansible scripts to deploy Artifactory using Docker, Podman, K8s and Yum on RHEL](https://gitlab.com/agravgaard/deploy-simulator)
 - [A Rust learning project, experimenting with options trading math](https://gitlab.com/agravgaard/algotrading)
 - [A tiny zero-downtime demo\* in Go with Kubernetes for teaching purposes](https://gitlab.com/agravgaard/ZeroDown)
 - [A Ray-tracing learning project in C++](https://gitlab.com/agravgaard/my-ray-tracer)
 - [A small Terraform demo\* connecting an AWS EC2 instance with Cloudflare DNS](https://gitlab.com/agravgaard/my-terraform-project)
 - [This resume, with a CI pipeline to check grammar, produce PDFs and publish online](https://gitlab.com/agravgaard/resume)
 - A few different R
   [1](https://gitlab.com/agravgaard/bayesiancourse2020),
   [2](https://gitlab.com/agravgaard/first_patient_dose_analysis),
   [3](https://gitlab.com/agravgaard/weplplotting_singlepatient),
   [4](https://gitlab.com/agravgaard/weplplotting),
   [5](https://gitlab.com/agravgaard/catphansegmentanalysis),
   [6](https://gitlab.com/agravgaard/gammastatistics),
   Python
   [1](https://gitlab.com/dcpt-research/pyvoistats),
   [2](https://gitlab.com/dcpt-research/doserecalc)
   and C++
   [1](https://github.com/agravgaard/QtPlotBraggPeak),
   [2](https://github.com/agravgaard/guiPMC)
   data analysis projects from my PhD
 - [My PhD Thesis in LaTeX, with a CI pipeline to check grammar and self-plagiarism and produce PDFs](https://gitlab.com/agravgaard/phd-thesis)
 - Various contributions include
   [the GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/1955),
   [ITK](https://github.com/InsightSoftwareConsortium/ITK/pull/2755),
   [RTK](https://github.com/SimonRit/RTK/pulls?q=is%3Apr+author%3Aagravgaard+is%3Aclosed),
   [DCMTK](https://github.com/DCMTK/dcmtk/pull/9) and
   [rocm-arch](https://github.com/rocm-arch/rocm-arch/pulls?q=is%3Apr+author%3Aagravgaard+is%3Aclosed)

\*Performed live for clients

<p class="new-page"></p>

## Publications

**Beam angle evaluation to improve inter-fraction motion robustness in pelvic lymph node irradiation with proton therapy**\
**Andreas G. Andersen**, Oscar Casares-Magaz, Jørgen B. B. Petersen, Jakob Toftegaard, Lise Bentzen, Sara Thörnqvist & Ludvig P. Muren
— *Acta Oncologica (2017), 56:6, 846-852, [doi-link](https://doi.org/10.1080/0284186X.2017.1317108)*

**Evaluation of an a priori scatter correction algorithm for cone-beam CT projections in photon vs. proton therapy gantries**\
**Andreas G. Andersen**, Yang-Kyun Park, Ulrik V. Elstrøm, Jørgen B. B. Petersen, Gregory C. Sharp, Brian Winey, Lei Dong & Ludvig P. Muren
— *Physics and Imaging in Radiation Oncology (2020), 16, 89-94, [doi-link](https://doi.org/10.1016/j.phro.2020.09.014)*

**Towards range-guidance in proton therapy to detect organ motion-induced dose degradations**\
Kia Busch\*, **Andreas G. Andersen**\*, Jørgen B. B. Petersen, Stine E. Petersen, Heidi S. Rønde, Lise Bentzen, Sara Pilskog, Peter Skyt, Ole Nørrevang & Ludvig P. Muren
— *Biomedical Physics & Engineering Express (2022), 8, 025018, [doi-link](https://doi.org/10.1088/2057-1976/ac5151)*\
\*Joint authorship

**A method for evaluation of proton plan robustness towards inter-fractional motion applied to pelvic lymph node irradiation**\
**Andreas G. Andersen**, Oscar Casares-Magaz, Ludvig P.  Muren, Jakob Toftegaard, Lise Bentzen, Sara Thörnqvist & Jørgen B. B. Petersen
— *Acta Oncologica (2015), 54:9, 1643-1650, [doi-link](https://doi.org/10.3109/0284186X.2015.1067720)*

**Evaluating the influence of organ motion during photon vs. proton therapy for locally advanced prostate cancer using biological models**\
Kia Busch, **Andreas G.  Andersen**, Oscar Casares-Magaz, Jørgen B. B. Petersen, Lise Bentzen, Sara Thörnqvist & Ludvig P. Muren
— *Acta Oncologica (2017), 56:6, 839-845, [doi-link](https://doi.org/10.1080/0284186X.2017.1317107)*

**A biological modelling based comparison of radiotherapy plan robustness using photons vs protons for focal prostate boosting**\
Jesper Pedersen, Oscar Casares-Magaz, Jørgen B. B. Petersen, Jarle Rørvik, Lise Bentzen, **Andreas G.  Andersen** & Ludvig P.Muren
— *Physics and Imaging in Radiation Oncology (2018), 6, 101-105, [doi-link](https://doi.org/10.1016/j.phro.2018.06.002.)*

**On-line dose-guidance to account for inter-fractional motion during proton therapy**\
Kia Busch, Ludvig P. Muren, Sara Thörnqvist, **Andreas G. Andersen**, Jesper Pedersen, Lei Dong & Jørgen B. B. Petersen
— *Physics and Imaging in Radiation Oncology (2019), 9, 7-13, [doi-link](https://doi.org/10.1016/j.phro.2018.11.009)*
